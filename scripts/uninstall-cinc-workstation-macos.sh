#!/bin/sh

if [ $(osascript -e 'application "Cinc Workstation" is running') = 'true' ]; then
  echo "Closing Cinc Workstation..."
  sudo osascript -e 'quit app "Cinc Workstation"' > /dev/null 2>&1;
fi
echo "Uninstalling Cinc Workstation..."
echo "  -> Removing files..."
sudo rm -rf '/opt/cinc-workstation'
sudo rm -rf '/Applications/Cinc Workstation.app'
echo "  -> Removing binary links in /usr/local/bin..."
sudo find /usr/local/bin -lname '/opt/cinc-workstation/*' -delete
echo "  -> Forgeting com.cinc-project.pkg.cinc-workstation package..."
sudo pkgutil --forget com.cinc-project.pkg.cinc-workstation > /dev/null 2>&1;
echo "Cinc Workstation Uninstalled."
