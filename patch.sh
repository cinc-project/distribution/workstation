#!/bin/bash -e
#
# Author:: Lance Albertson <lance@osuosl.org>
# Copyright:: Copyright 2020-2025, Cinc Project
# License:: Apache License, Version 2.0
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# This will patch a gem using Cinc branded patches
git_patch() {
  if [ -n "${2}" ] ; then
    CINC_BRANCH="${2}"
  elif [ "${REF}" == "main" -o -z "${REF}" ] ; then
    CINC_BRANCH="stable/cinc"
  else
    CINC_BRANCH="stable/cinc-${REF}"
  fi
  echo "Patching ${1} from ${CINC_BRANCH}..."
  git remote add -f --no-tags -t ${CINC_BRANCH} cinc https://gitlab.com/cinc-project/upstream/${1}.git
  git merge --no-edit cinc/${CINC_BRANCH}
}

export TOP_DIR="$(pwd)"
source /home/omnibus/load-omnibus-toolchain.sh
# remove any previous builds
rm -rf ${TOP_DIR}/chef-workstation
# Needed for git merge to function
git config --global user.email || git config --global user.email "maintainers@cinc.sh"
echo "Cloning ${REF:-main} branch from ${ORIGIN:-https://github.com/chef/chef-workstation.git}"
git clone -q -b ${REF:-main} ${ORIGIN:-https://github.com/chef/chef-workstation.git}
cd chef-workstation
git_patch chef-workstation ${CINC_REF}
cd $TOP_DIR
# Update Gemfile.lock for Cinc dependencies to allow bundle to work properly.
# The gem windows-pr is needed to properly fix the dependency issues
gem install -N bundler -v 2.3.7
cd ${TOP_DIR}/chef-workstation/components/gems
bundle _2.3.7_ lock --conservative --update \
  chef chef-apply chef-bin chef-cli chef-utils chef-zero inspec mixlib-install \
  knife \
  --add-platform ruby x64-mingw32 x86-mingw32 x64-mingw-ucrt
